//
// Created by anastasia on 23.09.20.
//

#include <limits>
#include "method4.h"

unsigned long x4;

void init4(long seed){
    x4 = seed;
}

long Reversed(long number, long p){
    if (number == 0){
        return std::numeric_limits<long>::max();
    }
    long i;
    for(i = 0; i<p; i++){
        if ((number*i)%p == 1){
            return i;
        }
    }
}

double ReversedCongruention()
{
    long a = 21;
    long c = 19;
    long p = 3571;

    x4 = (a*Reversed(x4, p) + c) % p;
    return double (x4)/p;
}