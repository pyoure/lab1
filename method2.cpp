//
// Created by anastasia on 23.09.20.
//

#include "method2.h"
unsigned long x2;

void init2(long seed){
    x2 = seed;
}

double SquareCongruention(){
    int a = 10;
    int c = 39;
    int d = 76;
    long m = 12345677;
    x2 = (d*x2*x2 + a*x2 + c)%m;
    return (double) x2 / m;
}