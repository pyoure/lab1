//
// Created by anastasia on 23.09.20.
//

#include "method5.h"
#include "method1.h"
#include "method2.h"


void init5(long seed){
    init1(seed);
    init2(seed);
}


double Unit(){
    long m = 22345786;
    long x5 = long ((LinealCongruention()*m-SquareCongruention())*m)%m;
    return double(x5)/m;
}