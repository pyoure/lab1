//
// Created by anastasia on 27.09.20.
//

#include "method8.h"
#include "method1.h"
#include "method2.h"
#include <cmath>

void init8(long seed){
    init1(seed);
    init2(seed);
}
double Correlation(){
    double u;
    double v;
    double x;

    do {
        do {
            u = LinealCongruention();
        }
        while (u==0);
        v = SquareCongruention();
        x = sqrt(8/exp(1))*(v - 0.5)/u;
    }
    while ((x >= 3 || x <= -3) || ((x*x > (5-4*exp(0.25))*u) && (x*x >= 4*exp(-1.35)/u + 1.4) && (x*x > -4*log(u))));
    //СПРОСИТЬ ПРО ВОЗРАЩЕНИЕ ДВУХ ЧИСЕЛ
    return (x +3)/6;
}

