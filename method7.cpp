//
// Created by anastasia on 24.09.20.
//

#include "method7.h"
#include "method1.h"
#include "method2.h"
#include <cmath>

void init7(long seed){
    init1(seed);
    init2(seed);
}
double PolarCoords() {
    double s;
    double v1;
    double v2;
    double res;
    do {
        v1 = 2 * LinealCongruention() - 1;
        v2 = 2 * SquareCongruention() - 1;
        s = v1 * v1 + v2 * v2;
        res = (v1+v2) / 2 * sqrt(((-2 * log(s)) / s));
    }
    while (s >= 1 || (res >= 3 || res <= -3));
    //СПРОСИТЬ ПРО ВОЗРАЩЕНИЕ ДВУХ ЧИСЕЛ
    return (res +3)/6;
}
