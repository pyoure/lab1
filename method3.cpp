//
// Created by anastasia on 23.09.20.
//

#include "method3.h"

unsigned long x3_1;
unsigned long x3_2 = 123566;

void init3(long seed){
    x3_1 = seed;
}

double FibonachiNumbers(){
    long m = 12345674;
    x3_2 = (x3_1+x3_2) % m;
    x3_1 = (x3_2 - x3_1) % m ;
    return (double) x3_2 / m;
}

