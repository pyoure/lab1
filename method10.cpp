//
// Created by anastasia on 27.09.20.
//

#include "method10.h"
#include "method1.h"
#include <cmath>
void init10(long seed){
    init1(seed);
}
double ArenthMethod(){
    double y;
    double x;
    double a = 0.9;
    double v;

    do{
        y = tan(M_PI*LinealCongruention());
        x = sqrt(2*a-1)*y +a -1;
        v = LinealCongruention();
    }
    while ((x<=0) && (v > (1 + y*y)*exp((a-1)*log(x/(a-1))-sqrt(2*a-1)*y)));
    return x;
}