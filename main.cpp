#include <iostream>
#include <ctime>
#include <functional>
#include <vector>
#include "method1.h"
#include "method2.h"
#include "method3.h"
#include "method4.h"
#include "method5.h"
#include "method6.h"
#include "method7.h"
#include "method8.h"
#include "method9.h"
#include "method10.h"
#include <cmath>


void test(std::function<double()> randomizer, std::function<void(long)> init ){
    std::vector<double> v(10,0);
    init(time(nullptr));
    for(int i = 0; i<30; i++)
        std::cout<< randomizer()<< ";" <<std::endl;
    int count_tests = 1000;
    for(int i = 0; i<count_tests; i++){
        double value = randomizer();
        if (value <= 0.1 && value>=0){
            v[0]++;
        }
        else if (value <= 0.2){
            v[1]++;
        }
        else if (value <= 0.3){
            v[2]++;
        }
        else if (value <= 0.4){
            v[3]++;
        }
        else if (value <= 0.5){
            v[4]++;
        }
        else if (value <= 0.6){
            v[5]++;
        }
        else if (value <= 0.7){
            v[6]++;
        }
        else if (value <= 0.8){
            v[7]++;
        }
        else if (value <= 0.9){
            v[8]++;
        }
        else if (value <= 1){
            v[9]++;
        }
    }
    double a = 0.0;
    double b = 0.1;
    for(int x = 0; x<10; x++) {
        std::cout << "[" << a << ";" << b << "]\t" << v[x] / count_tests << std::endl;
        a += 0.1;
        b += 0.1;
    }
}
int main(){
    test(ReversedCongruention, init4);
    return 0;
}


